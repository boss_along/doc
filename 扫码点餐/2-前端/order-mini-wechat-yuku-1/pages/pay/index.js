// pages/pay/index.js
import utils from '../../utils/util';
import uuid from '../../utils/uuid_v1';
import { CONFIG } from '../../utils/config';

let currentOutTradeNo = '';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    config: ''
  },

  goBack: function () {
    wx.navigateBack({
      delta: 1
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log('onLoad payment page', options);
    if (currentOutTradeNo === options.outTradeNo) {
      console.log('Same_OutTradeNo_Error!');
      wx.showToast({
        title: '支付异常，请稍后重试',
        icon: 'none',
        duration: 2000
      });
      const that = this;
      setTimeout(() => {
        that.goBack()
      }, 2000);
      return
    }
    currentOutTradeNo = options.outTradeNo;
    this.setData({
      config: options
    });
    setTimeout(() => {
      console.log('Requesting payment...');
      this.payBtn()
    }, 500);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    console.log('onReady payment page')
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    console.log('onShow payment page')
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    console.log('onHide payment page')
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    console.log('onUnload payment page')
  },

  // 支付按钮
  payBtn: function () {
    const that = this;
    wx.getSystemInfo({
      success: res => {
        if (res.language == "zh_CN" || res.language == "zh") {
          wx.showLoading({
            title: '支付中',
            mask: true,
          });
        } else {
          wx.showLoading({
            title: 'Paying',
            mask: true,
          });
        }
        console.log(res.language)
      },
    });
    const a = this.data.config;
    wx.requestPayment(
        {
          'timeStamp': a.timeStamp,
          'nonceStr': a.nonceStr,
          'package': "prepay_id=" + a.prepayId,
          'signType': 'MD5',
          'paySign': a.paySign,
          'success': function (res) {
            wx.hideLoading();
            that.request('SUCCESS');
          },
          'fail': function (res) {
            wx.hideLoading();
            console.log(res);
            that.request('FAIL');
          },
          'complete': function (res) {
            wx.hideLoading()
          }
        })
  },

  // 调后端接口
  request: function (state) {
    const that = this;
    const params = {
      action: state,
      timeStamp: new Date().getTime(),
      nonceStr: uuid(),
    };
    wx.request({
      url: `${CONFIG.url}/qrdish-api/trans-pay/${this.data.config.outTradeNo}?${utils.urlEncode(params).slice(1)}`,
      header: {
        QR: this.data.config.QR,
        AU: this.data.config.AU,
        PLAT: 'WECHAT_MA'
      },
      method: 'POST',
      dataType: 'json',
      responseType: 'text',
      success: function (res) {
        that.goBack()
      },
      fail: function (res) {
        console.log('fail', res)
      },
      complete: function (res) {
        console.log('complete', res)
      }
    })
  },
  fail: function () {
    this.request('FAIL');
  },
  success: function () {
    this.request('SUCCESS');
  }
});
