//index.js
//获取应用实例
import utils from '../../utils/util';
import {
  CONFIG
} from '../../utils/config';
import {
  getUserToken,
  getQrTokenByQr,
  getQrTokenByTid,
} from '../../services/jwt.js';
import {
  SUCCESSCODE
} from '../../services/servlets.js';
const app = getApp()
const AppData = app.globalData;

Page({
  data: {
    srcUrl: '',
    params: {},
    code: '',
    location: '',
    formId: '',
    enterText: '',
    scanText: '',
    preOrderText: '',
    page: 'null',
    showEnter: false,
    showScan: false,
    showPreOrder: false,
    enterType: 'default',
    wxUserInfo: undefined,
    exitConfirmVisible: false,
    exitConfirmTitle: '请扫其它桌台二维码下单~',
    exitConfirmExitName: '我知道了',
  },



  onLoad: function(options) {
    let params = app.globalData.params;
    const { page } = params;
    if(!page){
      if (options && options.exitTitle) {
        const decodeTitle = decodeURI(options.exitTitle);
        this.setData({
          exitConfirmVisible: true,
          exitConfirmTitle: decodeTitle
        });
      }
      this.getStoreInfo();
    }else{
      this.startGetLoginParams();
    }
  },

  async getStoreInfo() {
    const {
      cid,
      token,
      tid
    } = app.globalData.params;
    const params = {
      token,
      tenantId: tid,
      code: cid,
    };
    let res = {};
    if (!cid) {
      res = await getQrTokenByTid(params);
    } else {
      res = await getQrTokenByQr(params);
    }
    if (res && res.code === SUCCESSCODE) {
      if (!cid) {
        this.setStoreInfo({
          storeConf: res.data,
        });
      } else {
        this.setStoreInfo(res.data);
      }
    } else {
      utils.showToast("服务器在打瞌睡，请重新进入或联系服务员哦~");
      console.error('获取门店/商户信息失败 => ', res);
    }
  },

  setStoreInfo(storeAllInfo) {
    const {
      store,
      storeConf,
      jwt,
      table
    } = storeAllInfo;
    app.globalData.qrJwt = jwt;
    app.globalData.storeConfig = storeConf;
    app.globalData.storeInfo = store;
    app.globalData.table = table;
    if (this.checkStoreIsOpen()) {
      this.showControl();
    }
  },

  checkStoreIsOpen() {
    if (app.globalData.storeInfo) {
      // 返回是否营业 保证扫码进的是营业餐厅
      const {
        status
      } = app.globalData.storeInfo;
      if (!status) {
        wx.showModal({
          title: '',
          content: '抱歉，此门店暂未营业',
          success(res) {
            if (res.confirm) {
              console.log('需要实现退出');
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }
      return app.globalData.storeInfo.status;
    }
    return true;
  },

  showControl() {
    const isOpenPreOrder = utils.isOpenPreOrder(AppData);
    const isOrderToTable = utils.isOrderToTable(AppData);
    if (app.globalData.table || (!isOpenPreOrder && !isOrderToTable)) {
      this.setData({
        showEnter: true,
      });
    } else {
      this.setData({
        showScan: true,
        showPreOrder: isOpenPreOrder,
      });
    }
    this.startGetLoginParams();
  },

  startGetLoginParams() {
    console.log(app.globalData.params, 'index拿到url参数')
    let params = app.globalData.params;
    params.tid = CONFIG.tid;
    // params.tid = '5c82040e834e900001dbbef8';
    this.setData({
      params,
      page: params.page || ''
    });
    // 登录
    wx.login({
      success: res => {
        this.setData({
          code: res.code,
        });
        console.log(res.code);
        this.setUrl();
      }
    })
    this.getLocation();
    this.setLanguage();
  },

  setUrl() {
    if (this.data.code && this.data.location && (!this.data.page ? this.data.formId && this.data.wxUserInfo : true)) {
      wx.hideLoading();
      const params = {
        ...this.data.params,
        t: new Date().getTime(),
        code: this.data.code,
        formId: this.data.formId,
        enterType: this.data.enterType,
        ...this.data.location,
        ...this.data.wxUserInfo
      }
      console.log(params, 'url参数');
      const srcUrl = `qrdish.tst.choicesaas.cn/page/index.yuku.html?${utils.urlEncode(params).slice(1)}`;
      // const srcUrl = 'http://10.10.0.164:3000/index.yuku.html?' + utils.urlEncode(params).slice(1);
      console.log(srcUrl);
      this.setData({
        srcUrl,
      });
      app.globalData.srcUrl = srcUrl;
      wx.redirectTo({
        url: '/pages/webView/index'
      })
    }
  },

  getWelcomeTitle() {
    const table = app.globalData.table;
    const store = app.globalData.storeInfo;
    return store ? `${store.storeName}${table ? ` | ${table.tableName}` : ''}` : "欢迎进入扫码点餐系统";
  },

  setLanguage() {
    const welcomeText = this.getWelcomeTitle();
    wx.getSystemInfo({
      success: res => {
        if (res.language == "zh_CN" || res.language == "zh") {
          this.setData({
            welcomeText,
            enterText: "进入点餐",
            scanText: "扫码点餐",
            preOrderText: "预约点餐",
          })
        } else {
          this.setData({
            enterText: "Enter",
            welcomeText: "Scan your taste",
            scanText: "Scan code",
            preOrderText: "Reservation",
          })
        }
        console.log(res.language)
      },
    })
  },

  getLocation() {
    const fn = () => {
      wx.getLocation({
        type: 'gcj02',
        success: res => {
          console.log(res);
          const latitude = res.latitude
          const longitude = res.longitude
          const speed = res.speed
          const accuracy = res.accuracy
          console.log('wx获取地理位置信息成功');
          this.setData({
            location: {
              latitude,
              longitude,
              accuracy,
              hasLocation: true,
            }
          });
          this.setUrl();
        },
        fail: () => {
          wx.hideLoading();
          this.setData({
            location: {}
          });
          this.setUrl();
        }
      })
    }
    wx.getSetting({
      success: (res) => {
        console.log(res);
        if (!res.authSetting['scope.userLocation']) {
          wx.authorize({
            scope: 'scope.userLocation',
            success(res) {
              console.log(res);
              fn()
            },
            fail: () => {
              this.setData({
                location: {}
              });
              this.setUrl();
            },
            complete() {}
          })
        } else {
          fn()
        }
      }
    })
  },

  getWxUserInfo(e) {
    const {
      detail = {}
    } = e;
    const {
      userInfo
    } = detail;
    if (userInfo) {
      console.log(userInfo);
      const {
        nickName,
        avatarUrl
      } = userInfo;
      if (nickName && avatarUrl) {
        this.setData({
          wxUserInfo: {
            nickName,
            avatarUrl
          }
        });
        this.setUrl();
      }
    } else {
      utils.showToast('获取用户信息失败，请再次点击并允许获取用户信息');
    }
  },

  formSubmit(e) {
    wx.showLoading({
      mask: true
    });
    this.setData({
      formId: e.detail.formId
    });
    this.setUrl();
  },

  scanSubmit(e) {
    wx.scanCode({
      mode: 'scanCode',
      success: (res) => {
        const {
          result
        } = res;
        const params = utils.getUrlParam(result);
        console.log(params);
        if (params) {
          const {
            tid,
            cid,
            token
          } = params;
          const currentTid = AppData.params.tid;
          if (tid === currentTid && cid && token) {
            AppData.params = {
              ...AppData.params,
              ...params
            };
            this.setData({
              formId: e.detail.formId,
              params: {
                ...this.data.params,
                ...AppData.params
              }
            })
            this.setUrl();
          } else {
            utils.showToast('二维码信息有误，请扫桌台二维码下单~');
          }
        }
      },
      fail: (error) => {
        console.log(error);
      }
    });
  },

  preOrderSubmit(e) {
    this.setData({
      enterType: 'preOrder',
      formId: e.detail.formId
    });
    this.setUrl();
  },
})
