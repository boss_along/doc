import {
  CONFIG
} from './config';
import uuid from './uuid_v1';

function querystring(obj){
  return Object.keys(obj).reduce( (str, key, index) => {
    if(obj[key] !== undefined){
      return `${str}${index !== 0 ? '&' : ''}${key}=${obj[key]}`;
    }
    return str;
  }, '?')
}

/**
 * 判断请求状态是否成功
 * 参数：http状态码
 * 返回值：[Boolen]
 */
function isHttpSuccess(status) {
  return status >= 200 && status < 300 || status === 304;
}

const wxRequest = async(url, options, hideLoading) => {
  let newOptions;
  let newUrl = url;
  let responseBody = {};

  let {
    method = 'POST', body = {}, params = {}, headers = {}
  } = options || {};
  const appData = getApp().globalData;
  const qrJwt = appData.qrJwt;
  const userJwt = appData.userJwt;
  const shopInfo = appData.storeInfo;
  const shopId = shopInfo && shopInfo.store ? shopInfo.store.storeId : undefined;
  const tid = CONFIG.tid;
  let defaultHeaders = {
    QR: qrJwt,
    AU: userJwt,
    PLAT: 'WECHAT_MP',
    UUID: uuid(),
    TID: tid,
    SHOPID: shopId,
  };
  Object.keys(defaultHeaders).forEach((key) => {
    const v = defaultHeaders[key];
    if (!v) {
      delete defaultHeaders[key];
    }
  })
  const defaultParams = {
    timeStamp: new Date().getTime(),
    nonceStr: uuid(),
  };
  if (!(typeof body === 'string') && body) {
    newOptions = Object.assign({}, options, {
      body: body
    });
    responseBody = {
      credentials: 'same-origin',
      ...newOptions,
      headers: {
        ...defaultHeaders,
        ...headers,
      },
    };
  } else {
    newOptions = options;
    responseBody = {
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        ...defaultHeaders,
        ...headers,
      },
      ...newOptions,
    };
  }
  // hideLoading可以控制是否显示加载状态
  if (!hideLoading) {
    wx.showLoading({
      title: '加载中...',
    })
  };
  newUrl += querystring({ ...params, ...defaultParams });
  let res = await new Promise((resolve, reject) => {
    wx.request({
      url: newUrl,
      method: method,
      data: newOptions,
      header: headers,
      success: (res) => {
        if (res && res.data && isHttpSuccess(res.statusCode)) {
          resolve(res.data);
        } else {
          console.error(res);
          reject(res);
        }
      },
      fail: (err) => {
        reject(err);
      },
      complete: () => {
        wx.hideLoading();
      }
    });
  }).catch((e) => {
    wx.hideLoading();
    console.error(e);
    return e.data
  });
  return res;
}

export {
  wxRequest
}