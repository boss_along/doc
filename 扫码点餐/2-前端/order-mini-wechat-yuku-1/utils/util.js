const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}

// 获取url完整链接
let getQueryString = function (url, name) {
  // console.log("url = " + url)
  // console.log("name = " + name)
  var reg = new RegExp('(^|&|/?)' + name + '=([^&|/?]*)(&|/?|$)', 'i')
  var r = url.substr(1).match(reg)
  if (r != null) {
    // console.log("r = " + r)
    // console.log("r[2] = " + r[2])
    return r[2]
  }
  return null;
}

// url完整链接把参数转成对象
function parseQueryString(url) {
  var str = url.split("?")[1],    //通过?得到一个数组,取?后面的参数
    items = str.split("&");    //分割成数组
  var arr, name, value;

  for (var i = 0; i < items.length; i++) {
    arr = items[i].split("=");    //["key0", "0"]
    name = arr[0];
    value = arr[1];
    this[name] = value;
  }
}

// url参数对象形式转正常参数形式
var urlEncode = function (param, key, encode) {
  if (param == null) return '';
  var paramStr = '';
  var t = typeof (param);
  if (t == 'string' || t == 'number' || t == 'boolean') {
    paramStr += '&' + key + '=' + ((encode == null || encode) ? encodeURIComponent(param) : param);
  } else {
    for (var i in param) {
      var k = key == null ? i : key + (param instanceof Array ? '[' + i + ']' : '.' + i)
      paramStr += urlEncode(param[i], k, encode)
    }
  }
  return paramStr;
}

// 是否开启预点餐开关
function isOpenPreOrder(APPDATA) {
  return APPDATA.storeConfig && APPDATA.storeConfig.preOrderEnabled;
}

// 是否订单到桌台
function isOrderToTable(APPDATA) {
  return APPDATA.storeConfig && APPDATA.storeConfig.orderToTableEnabled;
}

// 获取url后参数
function getUrlParam(url) {
  var pattern = /(\w+)=(\w+)/ig;
  var parames = {};
  url.replace(pattern, function (a, b, c) {
    parames[b] = c;
  });
  return parames;
}

function showToast(title, type){
  wx.showToast({
    title,
    icon: type || 'none',
    duration: 2000
  });
}

module.exports = {
  formatTime: formatTime,
  getQueryString: getQueryString,
  parseQueryString: parseQueryString,
  urlEncode: urlEncode,
  isOpenPreOrder: isOpenPreOrder,
  isOrderToTable: isOrderToTable,
  getUrlParam: getUrlParam,
  showToast: showToast
}
