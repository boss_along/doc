// 发布前记得修改project.config.json appid
// pre:wxdb3d586127fcf40a
// tst:wxd79ad60821771906

const ENV ="dev";
const apiPrefix = '/qrdish-api';

const envConfig = {
  dev: {
    //url: `qrdish.dev.choicesaas.cn`,
    url: `x9dyc9.natappfree.cc`,
    tid: '5b03e065834e900001fd125c'
  },
  // tst: {
  //   url: 'https://qrdish.tst.choicesaas.cn',
  //   tid: '5cda625d28c5710001eef9ac'
  // },
  // pre: {
  //   url:'https://qrdish.pre.choicesaas.cn',
  //   tid: '5b076cd928ff70000165a081'
  // },
  // stable: {
  //   url: 'https://qrdish.choicesaas.cn',
  //   tid: ''
  // }
}[ENV];

export const CONFIG = { ...envConfig, apiPrefix};
