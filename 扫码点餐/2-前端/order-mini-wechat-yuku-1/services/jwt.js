import { wxRequest } from '../utils/request.js';
import { PUBLIC } from './servlets';

/**
 * 获取用户token
 */
async function getUserToken({ authInfo }) {
  return wxRequest(PUBLIC.AUTH.url, Object.assign({}, PUBLIC.AUTH.extra, { params: { authInfo } }));
}

/**
 * 根据二维码信息获取门店信息及二维码token
 */
async function getQrTokenByQr(params) {
  return wxRequest(`${PUBLIC.QR_TOKEN_BY_QR.url}/${params.tenantId}/${params.token}/${params.code}`);
}

/**
 * 根据商户id获取商户信息
 */
async function getQrTokenByTid(params) {
  return wxRequest(`${PUBLIC.GET_TENANT_CONFIG.url}`, { params: { tenantId: params.tenantId } });
}

export{
  getUserToken,
  getQrTokenByQr,
  getQrTokenByTid
}