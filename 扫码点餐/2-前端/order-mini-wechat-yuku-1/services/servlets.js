import { CONFIG } from '../utils/config.js';

const SUCCESSCODE = "10000";

const PUBLIC = {
  // save form id
  SAVE_FORM: { url: `${CONFIG.url}${CONFIG.apiPrefix}/ma/formid` },
  // 获取用户token
  AUTH: { url: `${CONFIG.url}${CONFIG.apiPrefix}/auth`, extra: { method: 'GET' } },
  // 根据二维码信息获取门店信息及二维码token
  QR_TOKEN_BY_QR: { url: `${CONFIG.url}${CONFIG.apiPrefix}/qr` },
  // 根据门店获取门店信息及二维码token
  QR_TOKEN_BY_STORE: { url: `${CONFIG.url}${CONFIG.apiPrefix}/qr/store`, extra: { method: 'GET' } },
  // 获取用户信息(门店菜单)
  GET_USER_INFO: { url: `${CONFIG.url}${CONFIG.apiPrefix}/information/userId` },
  // 获取商户配置
  GET_TENANT_CONFIG: { url: `${CONFIG.url}${CONFIG.apiPrefix}/public/tenantConfig`}
};

export {
  PUBLIC,
  SUCCESSCODE
}