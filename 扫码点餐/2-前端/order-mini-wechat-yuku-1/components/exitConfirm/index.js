// components/customModal/index.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    visible: {
      type: Boolean,
      value: false,
    },
    headerTitle: {
      type: String,
      value: '温馨提示',
    },
    bodyText: {
      type: String,
      value: '您将退出小程序',
    },
    exitName: {
      type: String,
      value: '退出',
    },
  },

  /**
   * 组件的初始数据
   */
  data: {
  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})
