var utils = require('./utils/util')
import { CONFIG } from '/utils/config.js';
App({
  onLaunch: function(options) {},
  onShow: function(options) {
    let {
      q,
      ...query
    } = options.query;
    let params = {}
    q = q && decodeURIComponent(q)
    if (q) {
      params = {
        ...new utils.parseQueryString(q),
      }
    }
    if (query) {
      params = {
        ...params,
        ...query
      }
    }
    this.globalData.params = {
      ...params,
      tid: CONFIG.tid,
    };
    const newEnterScene = options.scene;
    if (this.isNeedReLoad(newEnterScene)) {
      this.reLoadApp();
    }
  },
  isNeedReLoad(newEnterScene) {
    const oldEnterScene = this.enterScene;
    if (!oldEnterScene) {
      this.enterScene = newEnterScene;
      return false;
    }
    if (oldEnterScene === newEnterScene) {
      return false;
    }
    // 当新或旧的进入方式通过小程序推送消息进入，并且与之前不一样 就重新打开小程序
    if (oldEnterScene === 1014 || newEnterScene === 1014) {
      this.enterScene = newEnterScene;
      return true
    }
  },
  reLoadApp: function() {
    console.log('reLoad');
    wx.reLaunch({
      url: '/pages/index/index',
    })
  },
  globalData: {
    params: {},
  },
  enterScene: undefined,
})
