/*==============================================================*/
/* Database name:  zjtblog                                      */
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2019/9/24 14:42:04                           */
/*==============================================================*/


drop database if exists zjtblog;

/*==============================================================*/
/* Database: zjtblog                                            */
/*==============================================================*/
create database zjtblog;

use zjtblog;

/*==============================================================*/
/* Table: base_dish_info                                        */
/*==============================================================*/
create table base_dish_info
(
   id                   int not null auto_increment comment '菜品ID',
   uid                  varchar(36) comment '业务ID',
   restrant_id          varchar(36) comment '店铺ID',
   name                 varchar(36) comment '菜品类别名称|菜品名称',
   pid                  varchar(36) comment '父ID',
   is_leaf              int(1) comment '是否叶子节点',
   type                 int(1) comment '类别',
   dish_pic             varchar(256) comment '菜品图片',
   dish_price           int comment '菜品价格 单位：分',
   dish_unit            varchar(36) comment '菜品单位',
   sts                  int comment '状态',
   creater              varchar(64) comment '创建人',
   create_time          datetime comment '创建时间',
   modifer              varchar(64) comment '修改人',
   modify_time          datetime comment '修改时间',
   primary key (id)
);

/*==============================================================*/
/* Table: base_order_detail                                     */
/*==============================================================*/
create table base_order_detail
(
   id                   int not null auto_increment comment '订单明细ID',
   uid                  varchar(36) comment '业务ID',
   order_id             varchar(36) comment '订单ID',
   goods_id             varchar(36) comment '物品ID',
   goods_name           varchar(64) comment '物品名称',
   goods_price          int comment '物品单价',
   goods_num            int comment '物品数量',
   goods_amt            int comment '物品金额',
   dis_type             int comment '优惠类型
            1-折扣
            2-满减',
   dis_info             varchar(36) comment '优惠方案',
   dis_amt              int comment '优惠金额',
   pay_amt              int comment '实付金额',
   sts                  int comment '状态',
   creater              varchar(64) comment '创建人',
   create_time          datetime comment '创建时间',
   modifer              varchar(64) comment '修改人',
   modify_time          datetime comment '修改时间',
   primary key (id)
);

/*==============================================================*/
/* Table: base_order_info                                       */
/*==============================================================*/
create table base_order_info
(
   id                   int not null auto_increment comment '订单ID',
   uid                  varchar(36) comment '业务ID',
   user_id              varchar(36) comment '用户ID',
   out_trade_no         varchar(64) comment '商户订单号',
   pay_amt              int comment '支付金额',
   dis_amt              int comment '优惠金额',
   total_amt            int comment '总金额',
   pay_channel          int comment '支付渠道',
   order_no             varchar(64) comment '订单号',
   order_date           date comment '订单日期',
   sts                  int comment '状态',
   creater              varchar(64) comment '创建人',
   create_time          datetime comment '创建时间',
   modifer              varchar(64) comment '修改人',
   modify_time          datetime comment '修改时间',
   primary key (id)
);

/*==============================================================*/
/* Table: base_restaurant_info                                  */
/*==============================================================*/
create table base_restaurant_info
(
   id                   int not null auto_increment comment '餐厅ID',
   uid                  varchar(36) comment '业务ID',
   university_id        varchar(36) comment '院校ID',
   rest_name            varchar(64) comment '餐厅名称',
   rest_phone           varchar(36) comment '电话',
   rest_desc            varchar(128) comment '简介',
   rest_logo            varchar(256) comment '品牌logo',
   rest_adress          varchar(256) comment '机构地址',
   start_time           datetime comment '营业开始时间',
   over_time            datetime comment '营业结束时间',
   sts                  int comment '状态',
   creater              varchar(64) comment '创建人',
   create_time          datetime comment '创建时间',
   modifer              varchar(64) comment '修改人',
   modify_time          datetime comment '修改时间',
   primary key (id)
);

/*==============================================================*/
/* Table: base_shoping_cart                                     */
/*==============================================================*/
create table base_shoping_cart
(
   id                   int not null auto_increment comment '购物车ID',
   uid                  varchar(36) comment '业务ID',
   user_id              varchar(36) comment '用户ID',
   dish_id              varchar(36) comment '菜品ID',
   dish_name            varchar(36) comment '菜品名称',
   dish_num             int comment '菜品数量',
   dish_price           int comment '菜品价格',
   sts                  int comment '状态',
   creater              varchar(64) comment '创建人',
   create_time          datetime comment '创建时间',
   modifer              varchar(64) comment '修改人',
   modify_time          datetime comment '修改时间',
   primary key (id)
);

/*==============================================================*/
/* Table: base_university_info                                  */
/*==============================================================*/
create table base_university_info
(
   id                   int not null auto_increment comment '院校ID',
   uid                  varchar(36) comment '业务ID',
   university_name      varchar(64) comment '院校名称',
   university_adress    varchar(256) comment '院校地址',
   university_desc      varchar(128) comment '院校信息',
   sts                  int comment '状态',
   creater              varchar(64) comment '创建人',
   create_time          datetime comment '创建时间',
   modifer              varchar(64) comment '修改人',
   modify_time          datetime comment '修改时间',
   primary key (id)
);

/*==============================================================*/
/* Table: base_user_university                                  */
/*==============================================================*/
create table base_user_university
(
   id                   int not null auto_increment comment '主键',
   uid                  varchar(36) comment '业务ID',
   user_id              varchar(36) default NULL comment '用户ID',
   org_id               varchar(36) comment '机构ID',
   sts                  int comment '状态',
   creater              varchar(64) comment '创建人',
   create_time          datetime comment '创建时间',
   modifer              varchar(64) comment '修改人',
   modify_time          datetime comment '修改时间',
   primary key (id)
);

/*==============================================================*/
/* Table: sys_dict                                              */
/*==============================================================*/
create table sys_dict
(
   id                   int not null auto_increment comment 'ID',
   uid                  varchar(36) comment '业务ID',
   dict_type_id         varchar(36) comment '字典类型ID',
   dict_key             varchar(36) comment '字典key',
   dict_val             varchar(36) comment '字典value',
   seq                  int comment '顺序',
   sts                  int comment '状态',
   creater              varchar(64) comment '创建人',
   create_time          datetime comment '创建时间',
   modifer              varchar(64) comment '修改人',
   modify_time          datetime comment '修改时间',
   primary key (id)
);

/*==============================================================*/
/* Table: sys_dict_type                                         */
/*==============================================================*/
create table sys_dict_type
(
   id                   int not null auto_increment comment 'ID',
   uid                  varchar(36) comment '业务ID',
   dict_type_code       varchar(36) comment '字典类型编码',
   dict_type_desc       varchar(36) comment '字典类型描述',
   sts                  int comment '状态',
   creater              varchar(64) comment '创建人',
   create_time          datetime comment '创建时间',
   modifer              varchar(64) comment '修改人',
   modify_time          datetime comment '修改时间',
   primary key (id)
);

/*==============================================================*/
/* Table: sys_menu                                              */
/*==============================================================*/
create table sys_menu
(
   id                   int not null auto_increment comment '主键',
   uid                  varchar(36) comment '业务ID',
   icon                 varchar(100) default NULL comment '菜单图标',
   name                 varchar(50) default NULL comment '菜单名称',
   state                int(6) default NULL comment '菜单等级',
   url                  varchar(200) default NULL comment '菜单url',
   p_id                 varchar(36) default NULL comment '上级菜单ID',
   p_ids                varchar(50) comment '上级菜单json结构',
   type                 int comment '菜单类别',
   is_leaf              boolean comment '是否叶子节点',
   seq                  int(6) comment '展示顺序',
   permission           int comment '是否控制权限
            1-是
            2-否',
   sts                  int comment '状态
            0-删除
            1-正常
            2-禁用
            ',
   creater              varchar(64) comment '创建人',
   create_time          datetime comment '创建时间',
   modifer              varchar(64) comment '修改人',
   modify_time          datetime comment '修改时间',
   primary key (id)
)
ENGINE=InnoDB AUTO_INCREMENT=1705032705 DEFAULT CHARSET=utf8;

/*==============================================================*/
/* Table: sys_role                                              */
/*==============================================================*/
create table sys_role
(
   id                   int not null auto_increment comment '主键',
   uid                  varchar(36) comment '业务ID',
   bz                   varchar(128) default NULL comment '角色名称',
   name                 varchar(256) default NULL comment '角色描述',
   sts                  int comment '状态',
   creater              varchar(64) comment '创建人',
   create_time          datetime comment '创建时间',
   modifer              varchar(64) comment '修改人',
   modify_time          datetime comment '修改时间',
   primary key (id)
)
ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

/*==============================================================*/
/* Table: sys_role_menu                                         */
/*==============================================================*/
create table sys_role_menu
(
   id                   int not null auto_increment comment '主键',
   uid                  varchar(36) comment '业务ID',
   role_id              varchar(36) default NULL comment '角色ID',
   menu_id              varchar(36) default NULL comment '菜单ID',
   sts                  int comment '状态',
   creater              varchar(64) comment '创建人',
   create_time          datetime comment '创建时间',
   modifer              varchar(64) comment '修改人',
   modify_time          datetime comment '修改时间',
   primary key (id)
)
ENGINE=InnoDB AUTO_INCREMENT=289 DEFAULT CHARSET=utf8;

/*==============================================================*/
/* Table: sys_user                                              */
/*==============================================================*/
create table sys_user
(
   id                   int not null auto_increment comment '主键',
   uid                  varchar(36) comment '业务ID',
   username             varchar(36) default NULL comment '用户名',
   password             varchar(200) default NULL comment '密码',
   true_name            varchar(50) default NULL comment '真实姓名',
   pic                  varchar(256) comment '头像',
   email                varchar(50) default NULL comment '邮箱',
   phone                varchar(20) default NULL comment '手机号',
   sex                  int comment '性别',
   birthday             date comment '生日',
   sts                  int comment '状态',
   creater              varchar(64) comment '创建人',
   create_time          datetime comment '创建时间',
   modifer              varchar(64) comment '修改人',
   modify_time          datetime comment '修改时间',
   primary key (id)
)
ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

/*==============================================================*/
/* Table: sys_user_role                                         */
/*==============================================================*/
create table sys_user_role
(
   id                   int not null auto_increment comment '主键',
   uid                  varchar(36) comment '业务ID',
   user_id              varchar(36) default NULL comment '用户ID',
   role_id              varchar(36) default NULL comment '角色ID',
   sts                  int comment '状态',
   creater              varchar(64) comment '创建人',
   create_time          datetime comment '创建时间',
   modifer              varchar(64) comment '修改人',
   modify_time          datetime comment '修改时间',
   primary key (id)
)
ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;

