/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50725
 Source Host           : localhost:3306
 Source Schema         : zjtblog

 Target Server Type    : MySQL
 Target Server Version : 50725
 File Encoding         : 65001

 Date: 25/01/2019 20:12:12
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_menu`;
CREATE TABLE `t_menu`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单图标',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单名称',
  `state` int(6) NULL DEFAULT NULL COMMENT '菜单等级',
  `url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单url',
  `p_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '上级菜单ID',
  `is_leaf` tinyint(1) NULL DEFAULT NULL COMMENT '是否叶子节点',
  `seq` int(6) NULL DEFAULT NULL COMMENT '展示顺序',
  `status` int(11) NULL DEFAULT NULL COMMENT '状态',
  `creater` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `modifer` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `modify_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_menu
-- ----------------------------
INSERT INTO `t_menu` VALUES ('1', '&#xe68e;', '系统菜单', 1, NULL, '-1', 1, 1, 0, NULL, NULL, NULL, NULL);
INSERT INTO `t_menu` VALUES ('10', '&#xe68e;', '内容管理', 1, NULL, '-1', 0, 2, 0, NULL, NULL, NULL, NULL);
INSERT INTO `t_menu` VALUES ('1000', 'icon-text', '文章管理', 2, 'https://www.hongxiu.com/', '10', 1, 1, 0, NULL, NULL, NULL, NULL);
INSERT INTO `t_menu` VALUES ('200000', '44', '44', 3, '44', '2000', 1, 1, 0, NULL, NULL, NULL, NULL);
INSERT INTO `t_menu` VALUES ('60', '&#xe631;', '系统管理', 1, NULL, '-1', 0, 3, 0, NULL, NULL, NULL, NULL);
INSERT INTO `t_menu` VALUES ('6000', '&#xe631;', '菜单管理', 2, 'admin/menu/tomunemanage', '60', 1, 1, 0, NULL, NULL, NULL, NULL);
INSERT INTO `t_menu` VALUES ('6010', 'icon-icon10', '角色管理', 2, 'admin/role/torolemanage', '60', 0, 2, 0, NULL, NULL, NULL, NULL);
INSERT INTO `t_menu` VALUES ('601010', 'icon-icon10', '三级测试菜单', 3, 'admin/role/torolemanage', '6010', 1, 1, 0, NULL, NULL, NULL, NULL);
INSERT INTO `t_menu` VALUES ('6020', '&#xe612;', '用户管理', 2, 'tUser/index', '60', 1, 3, 0, NULL, NULL, NULL, NULL);
INSERT INTO `t_menu` VALUES ('6030', '&#xe631;', 'sql监控', 2, 'druid/index.html', '60', 1, 4, 0, NULL, NULL, NULL, NULL);
INSERT INTO `t_menu` VALUES ('6040', 'icon-ziliao', '修改密码', 2, 'admin/user/toUpdatePassword', '60', 1, 5, 0, NULL, NULL, NULL, NULL);
INSERT INTO `t_menu` VALUES ('6050', 'icon-tuichu', '安全退出', 2, 'user/logout', '60', 1, 6, 0, NULL, NULL, NULL, NULL);
INSERT INTO `t_menu` VALUES ('61', '&#xe705;', '新闻资讯', 1, 'http://www.ifeng.com/', '-1', 1, 4, 0, NULL, NULL, NULL, NULL);
INSERT INTO `t_menu` VALUES ('6100', 'icon-text', '凤凰网', 2, 'http://www.ifeng.com/', '61', 1, 1, 0, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for t_role
-- ----------------------------
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `bz` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色名称',
  `name` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色描述',
  `status` int(11) NULL DEFAULT NULL COMMENT '状态',
  `creater` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `modifer` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `modify_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_role
-- ----------------------------
INSERT INTO `t_role` VALUES ('1', '最高权限', '管理员', 0, NULL, '2019-01-24 15:06:28', NULL, '2019-01-24 15:06:32');
INSERT INTO `t_role` VALUES ('2', '第二权限', '管理员助手', 0, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for t_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_role_menu`;
CREATE TABLE `t_role_menu`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `role_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色ID',
  `menu_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单ID',
  `status` int(11) NULL DEFAULT NULL COMMENT '状态',
  `creater` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `modifer` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `modify_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_role_menu
-- ----------------------------
INSERT INTO `t_role_menu` VALUES ('1', '1', '1', 0, NULL, NULL, NULL, NULL);
INSERT INTO `t_role_menu` VALUES ('10', '1', '6040', 0, NULL, NULL, NULL, NULL);
INSERT INTO `t_role_menu` VALUES ('11', '1', '6050', 0, NULL, NULL, NULL, NULL);
INSERT INTO `t_role_menu` VALUES ('12', '1', '6100', 0, NULL, NULL, NULL, NULL);
INSERT INTO `t_role_menu` VALUES ('13', '1', '200000', 0, NULL, NULL, NULL, NULL);
INSERT INTO `t_role_menu` VALUES ('14', '1', '601010', 0, NULL, NULL, NULL, NULL);
INSERT INTO `t_role_menu` VALUES ('15', '2', '1', 0, NULL, NULL, NULL, NULL);
INSERT INTO `t_role_menu` VALUES ('2', '1', '10', 0, NULL, NULL, NULL, NULL);
INSERT INTO `t_role_menu` VALUES ('3', '1', '60', 0, NULL, NULL, NULL, NULL);
INSERT INTO `t_role_menu` VALUES ('4', '1', '61', 0, NULL, NULL, NULL, NULL);
INSERT INTO `t_role_menu` VALUES ('5', '1', '1000', 0, NULL, NULL, NULL, NULL);
INSERT INTO `t_role_menu` VALUES ('6', '1', '6000', 0, NULL, NULL, NULL, NULL);
INSERT INTO `t_role_menu` VALUES ('7', '1', '6010', 0, NULL, NULL, NULL, NULL);
INSERT INTO `t_role_menu` VALUES ('8', '1', '6020', 0, NULL, NULL, NULL, NULL);
INSERT INTO `t_role_menu` VALUES ('9', '1', '6030', 0, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `username` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `true_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '真实姓名',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `status` int(11) NULL DEFAULT NULL COMMENT '状态',
  `creater` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `modifer` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `modify_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('1', 'admin', '1', '张三', 'boss_along@sina.com', '15652597360', 0, NULL, '2019-01-24 14:59:54', NULL, '2019-01-24 14:59:57');

-- ----------------------------
-- Table structure for t_user_role
-- ----------------------------
DROP TABLE IF EXISTS `t_user_role`;
CREATE TABLE `t_user_role`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `user_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户ID',
  `role_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色ID',
  `status` int(11) NULL DEFAULT NULL COMMENT '状态',
  `creater` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `modifer` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `modify_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_user_role
-- ----------------------------
INSERT INTO `t_user_role` VALUES ('1', '1', '1', 0, NULL, '2019-01-24 15:06:52', NULL, '2019-01-24 15:06:55');
INSERT INTO `t_user_role` VALUES ('2', '1', '2', 0, NULL, NULL, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
