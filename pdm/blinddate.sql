/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2019/8/19 10:49:09                           */
/*==============================================================*/


drop table if exists base_carousel;

drop table if exists user_base_info;

drop table if exists user_wife_condition;

/*==============================================================*/
/* Table: base_carousel                                         */
/*==============================================================*/
create table base_carousel
(
   id                   varchar(36) not null comment '主键',
   img_desc             varchar(64) comment '描述',
   url                  varchar(512) comment '路径',
   seq                  int comment '顺序',
   creater              varchar(64) comment '创建人',
   create_time          datetime comment '创建时间',
   modifer              varchar(64) comment '修改人',
   modify_time          datetime comment '修改时间',
   status               int(1) comment '状态',
   primary key (id)
);

/*==============================================================*/
/* Table: user_base_info                                        */
/*==============================================================*/
create table user_base_info
(
   id                   varchar(36) not null comment '主键',
   user_id              varchar(36) comment '用户ID',
   user_name            varchar(36) comment '用户名',
   sex                  int(1) comment '性别',
   marital_status       int(1) comment '婚姻状态',
   age                  int(2) comment '年龄',
   grade                int(1) comment '学历',
   hight                int(3) comment '身高',
   area                 varchar(36) comment '地区',
   income_type          int(1) comment '收入类型',
   income               decimal(8,0) comment '收入金额',
   creater              varchar(64) comment '创建人',
   create_time          datetime comment '创建时间',
   modifer              varchar(64) comment '修改人',
   modify_time          datetime comment '修改时间',
   status               int(1) comment '状态',
   primary key (id)
);

/*==============================================================*/
/* Table: user_wife_condition                                   */
/*==============================================================*/
create table user_wife_condition
(
   id                   varchar(36) not null comment '主键',
   user_id              varchar(36) comment '用户ID',
   girlfriend_sex       int(1) comment '性别',
   girlfriend_          varchar(256) comment '长相要求',
   girlfriend_age       varchar(3) comment '年龄',
   girlfriend_hight     int(3) comment '身高',
   girlfrient_type      int(2) comment '交友类型',
   girlfriend_married   int(1) comment '婚姻状况',
   girlfriend_edu_level int(1) comment '学历要求',
   credit_level         int(2) comment '诚信星级',
   girlfriend_area      varchar(64) comment '地区要求',
   creater              varchar(64) comment '创建人',
   create_time          datetime comment '创建时间',
   modifer              varchar(64) comment '修改人',
   modify_time          datetime comment '修改时间',
   status               int(1) comment '状态',
   primary key (id)
);

