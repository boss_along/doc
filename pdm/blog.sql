/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2019/5/23 10:53:50                           */
/*==============================================================*/


drop table if exists prod_material_return_in;

drop table if exists prod_material_return_in_detail;

drop table if exists prod_material_return_in_detail_batch;

drop table if exists prod_material_return_in_detail_date;

/*==============================================================*/
/* Table: prod_material_return_in                               */
/*==============================================================*/
create table prod_material_return_in
(
   sid                  bigint(20) not null auto_increment comment '自增主键',
   id                   varchar(36) default NULL comment '业务ID',
   tenant_id            varchar(36) default NULL comment '租户ID',
   quality_period_flag  tinyint(1) comment '全局保质期开关',
   bill_no              varchar(30) default NULL comment '领料退库单号',
   bill_type            smallint(6) default NULL comment '单据类型',
   bill_source          tinyint(4) default NULL comment '单据来源',
   up_bill_id           varchar(36) default NULL comment '上游单据ID',
   up_bill_no           varchar(30) default NULL comment '上游单据编号',
   up_bill_type         smallint(6) default NULL comment '上游单据类型',
   buss_date            date default NULL comment '业务日期',
   bill_status          smallint(6) default NULL comment '单据状态',
   total_amt            decimal(19,2) default NULL comment '总金额',
   total_amt_notax      decimal(19,2) default NULL comment '无税总金额',
   stock_in_org_id      varchar(36) default NULL comment '退入机构ID',
   stock_in_org_name    varchar(60) default NULL comment '退入机构名称',
   stock_in_org_type    smallint(6) default NULL comment '退入机构类型',
   stock_in_depot_id    varchar(36) default NULL comment '退入仓库ID',
   stock_in_depot_name  varchar(60) default NULL comment '退入仓库名称',
   stock_out_org_id     varchar(36) default NULL comment '退出机构ID',
   stock_out_org_name   varchar(60) default NULL comment '退出机构名称',
   stock_out_org_type   smallint(6) default NULL comment '退出仓库ID',
   stock_out_central_kitchen_id varchar(36) default NULL comment '退出中央厨房ID',
   stock_out_central_kitchen_name varchar(60) default NULL comment '退出中央厨房名称',
   create_user          varchar(36) default NULL comment '创建人ID',
   create_user_name     varchar(60) default NULL comment '创建人姓名',
   create_time          datetime default NULL comment '创建时间',
   update_user          varchar(36) default NULL comment '编辑人ID',
   update_user_name     varchar(60) default NULL comment '编辑人姓名',
   update_time          datetime default NULL comment '编辑时间',
   audit_user           varchar(36) default NULL comment '审核人ID',
   audit_user_name      varchar(60) default NULL comment '审核人姓名',
   audit_time           datetime default NULL comment '审核时间',
   audit_opinion        varchar(100) default NULL comment '审核意见',
   version              int(11) default NULL comment '版本号',
   remarks              varchar(200) default NULL comment '备注',
   delete_flag          tinyint(4) default NULL comment '删除标识',
   Column_38            varchar(60) comment '退出仓库名称',
   Column_39            char(10) comment '存下游单据ID',
   primary key (sid),
   unique key AK_UNIQUE_MATERIAL_RETURN_IN_ID (id)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='生产加工-领料退库主单据';

/*==============================================================*/
/* Table: prod_material_return_in_detail                        */
/*==============================================================*/
create table prod_material_return_in_detail
(
   sid                  bigint(20) not null auto_increment comment '自增主键',
   id                   varchar(36) default NULL comment '业务ID',
   bill_id              varchar(30) default NULL comment '领料退库单据ID',
   goods_id             varchar(36) default NULL comment '物品ID',
   goods_code           varchar(30) default NULL comment '物品编码',
   goods_name           varchar(60) default NULL comment '物品名称',
   goods_spec           varchar(60) default NULL comment '物品规格',
   unit_id              varchar(36) default NULL comment '物品标准单位ID',
   unit_name            varchar(60) default NULL comment '物品标准单位名称',
   unit_price           decimal(19,8) default NULL comment '物品标准单位单价',
   goods_qty            decimal(19,8) default NULL comment '物品标准单位数量',
   goods_amt            decimal(19,2) default NULL comment '物品金额',
   tax_ratio            decimal(6,2) default NULL comment '物品税率',
   unit_price_notax     decimal(19,8) default NULL comment '物品无税单价',
   goods_amt_notax      decimal(19,2) default NULL comment '物品无税金额',
   goods_dual_unit_id   varchar(36) default NULL comment '物品辅助单位ID',
   goods_dual_unit_name varchar(60) default NULL comment '物品辅助单位名称',
   goods_dual_qty       decimal(19,8) default NULL comment '物品辅助单位数量',
   quality_period_flag  tinyint(1) default NULL comment '物品是否启用保质期标识',
   create_user          varchar(36) default NULL comment '创建人ID',
   create_user_name     varchar(60) default NULL comment '创建人姓名',
   create_time          datetime default NULL comment '创建时间',
   update_user          varchar(36) default NULL comment '编辑人ID',
   update_user_name     varchar(60) default NULL comment '编辑人姓名',
   update_time          datetime default NULL comment '编辑时间',
   remarks              varchar(200) default NULL comment '备注',
   tenant_id            varchar(36) default NULL comment '租户ID',
   delete_flag          tinyint(4) default NULL comment '删除标识',
   primary key (sid),
   unique key AK_UNIQUE_MATERIAL_RETURN_IN_DETAIL_ID (id)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='生产加工-领料退库单据明细';

/*==============================================================*/
/* Table: prod_material_return_in_detail_batch                  */
/*==============================================================*/
create table prod_material_return_in_detail_batch
(
   sid                  bigint(20) not null auto_increment comment '自增主键',
   id                   varchar(36) default NULL comment '业务ID',
   bill_id              varchar(36) default NULL comment '领料退库单据ID',
   bill_detail_id       varchar(30) default NULL comment '领料退库单据明细ID',
   quality_period_id    varchar(36) comment '保质期表id',
   goods_id             varchar(36) default NULL comment '物品ID',
   goods_unit_price     decimal(19,8) default NULL comment '物品标准单位单价',
   goods_qty            decimal(19,8) default NULL comment '物品标准单位数量',
   goods_amt            decimal(19,2) default NULL comment '物品金额',
   goods_tax_ratio      decimal(6,2) default NULL comment '物品税率',
   goods_unit_price_notax decimal(19,8) default NULL comment '物品无税单价',
   goods_amt_notax      decimal(19,2) default NULL comment '物品无税金额',
   goods_dual_qty       decimal(19,8) default NULL comment '物品辅助单位数量',
   prod_date            date default NULL comment '生产日期',
   quality_period       int(11) default NULL comment '保质期（天）',
   exp_date             date default NULL comment '过期日期',
   batch_id             varchar(36) default NULL comment '批次ID',
   batch_no             varchar(36) default NULL comment '批次号',
   create_time          datetime default NULL comment '创建时间',
   tenant_id            varchar(36) default NULL comment '租户ID',
   delete_flag          tinyint(4) default NULL comment '删除标识',
   primary key (sid),
   unique key AK_UNIQUE_MATERIAL_RETURN_IN_DETAIL_BATCH_ID (id)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='生产加工-领料退库单据明细项';

/*==============================================================*/
/* Table: prod_material_return_in_detail_date                   */
/*==============================================================*/
create table prod_material_return_in_detail_date
(
   sid                  bigint(20) not null auto_increment comment '自增主键',
   id                   varchar(36) default NULL comment '业务ID',
   bill_id              varchar(30) default NULL comment '领料退库单据ID',
   bill_detail_id       varchar(30) default NULL comment '领料退库单据明细ID',
   goods_id             varchar(36) default NULL comment '物品ID',
   goods_unit_price     decimal(19,8) default NULL comment '物品标准单位单价',
   goods_qty            decimal(19,8) default NULL comment '物品标准单位数量',
   goods_amt            decimal(19,2) default NULL comment '物品金额',
   goods_tax_ratio      decimal(6,2) default NULL comment '物品税率',
   goods_unit_price_notax decimal(19,8) default NULL comment '物品无税单价',
   goods_amt_notax      decimal(19,2) default NULL comment '物品无税金额',
   goods_dual_qty       decimal(19,8) default NULL comment '物品辅助单位数量',
   pro_date             date default NULL comment '生产日期',
   quality_period       int(11) default NULL comment '保质期（天）',
   exp_date             date default NULL comment '过期日期',
   create_user          varchar(36) default NULL comment '创建人ID',
   create_user_name     varchar(60) default NULL comment '创建人姓名',
   create_time          datetime default NULL comment '创建时间',
   update_user          varchar(36) default NULL comment '编辑人ID',
   update_user_name     varchar(60) default NULL comment '编辑人姓名',
   update_time          datetime default NULL comment '编辑时间',
   remarks              varchar(200) default NULL comment '备注',
   tenant_id            varchar(36) default NULL comment '租户ID',
   delete_flag          tinyint(4) default NULL comment '删除标识',
   primary key (sid),
   unique key AK_UNIQUE_MATERIAL_RETURN_IN_DETAIL_DATE_ID (id)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='生产加工-领料退库单据明细项';

