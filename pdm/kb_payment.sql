/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2019/9/27 10:54:47                           */
/*==============================================================*/


drop table if exists kb_pay_account;

drop table if exists kb_pay_extra_text;

drop table if exists kb_pay_order;

drop table if exists kb_pay_order_refund;

drop table if exists kb_pay_order_refund_state;

drop table if exists kb_pay_order_result;

drop table if exists kb_pay_order_state;

drop table if exists kb_pay_refund_apply;

drop table if exists kb_pay_trade_no;

drop table if exists mch_p_order;

drop table if exists mp_account;

drop table if exists mp_account_channel;

drop table if exists mp_account_store;

drop table if exists mp_aggregator;

drop table if exists mp_channel;

drop table if exists mp_channel_aggregator;

drop table if exists mp_isv;

/*==============================================================*/
/* Table: kb_pay_account                                        */
/*==============================================================*/
create table kb_pay_account
(
   id                   bigint(20) unsigned not null auto_increment,
   pay_aggregator       varchar(32) character set utf8,
   channels             varchar(64) character set utf8,
   mch_no               int(11) not null,
   configure            text character set utf8,
   create_time          datetime,
   update_time          datetime,
   deleted              tinyint(1) not null,
   primary key (id)
)
ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

/*==============================================================*/
/* Table: kb_pay_extra_text                                     */
/*==============================================================*/
create table kb_pay_extra_text
(
   id                   int(11) unsigned not null auto_increment comment '主键',
   deleted              char(1) character set utf8 comment '删除标志',
   create_time          datetime comment '创建时间',
   update_time          datetime comment '修改时间',
   inst_payee_account_no varchar(64) character set utf8 comment '支付机构收款账号',
   out_trade_no         varchar(64) character set utf8 comment '商户订单号',
   ass_pk               int(11) unsigned not null comment '关联数据ID',
   key                  varchar(20) character set utf8 comment '扩展属性key',
   text                 text character set utf8 comment '扩展属性text数据',
   primary key (id),
   unique key uk_trade_key (key, ass_pk),
   key idx_text (out_trade_no, key, deleted)
)
ENGINE = InnoDB AUTO_INCREMENT = 6462 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '订单text类型属性扩展表' ROW_FORMAT = Dynamic;

/*==============================================================*/
/* Table: kb_pay_order                                          */
/*==============================================================*/
create table kb_pay_order
(
   id                   bigint(20) unsigned not null auto_increment comment '主键',
   deleted              char(1) character set utf8 comment '删除标志',
   create_time          datetime comment '创建时间',
   update_time          datetime comment '修改时间',
   pay_inst             varchar(32) comment '支付机构',
   inst_payee_account_no varchar(64) character set utf8 comment '支付机构收款账号',
   body                 varchar(255) character set utf8 comment '商品描述',
   out_trade_no         varchar(64) character set utf8 comment '商户订单号',
   total_fee            int(10) unsigned not null default 0 comment '支付金额,单位:分',
   discountable_amount  int(10) unsigned not null default 0 comment '可优惠金额,单位:分',
   time_expire          varchar(14) comment '交易结束时间',
   trade_type           varchar(20) character set utf8 comment '交易类型',
   pay_channel          varchar(10) character set utf8 comment '支付渠道',
   currency_type        varchar(16) comment '货币类型
            CNY-人民币',
   auth_code            varchar(128) comment '授权码',
   scene                varchar(32) comment '支付场景
            条码支付，取值：bar_code 
            声波支付，取值：wave_code',
   order_date           varchar(20) character set utf8 comment '订单日期',
   order_no             varchar(64) character set utf8 comment '订单号',
   store_code           varchar(64) character set utf8 comment '门店编码',
   primary key (id),
   key idx_order (inst_payee_account_no, out_trade_no, deleted),
   key idx_order_cds (inst_payee_account_no, store_code, out_trade_no)
)
ENGINE = InnoDB AUTO_INCREMENT = 7522 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '支付订单' ROW_FORMAT = Dynamic;

/*==============================================================*/
/* Table: kb_pay_order_refund                                   */
/*==============================================================*/
create table kb_pay_order_refund
(
   id                   int(11) unsigned not null auto_increment comment '主键',
   deleted              char(1) character set utf8 comment '删除标志',
   create_time          datetime comment '创建时间',
   update_time          datetime comment '修改时间',
   pay_inst             varchar(32) comment '支付机构',
   inst_payee_account_no varchar(64) character set utf8 comment '支付机构收款账号',
   transaction_id       varchar(32) comment '支付机构订单号',
   out_trade_no         varchar(64) character set utf8 comment '商户订单号',
   out_refund_no        varchar(64) character set utf8 comment '商户退款单号',
   total_fee            int(10) unsigned not null default 0 comment '支付金额,单位:分',
   refund_fee           int(10) not null comment '申请退款金额:分',
   refund_desc          varchar(255) character set utf8 comment '退款原因',
   refund_account       varchar(30) comment '退款资金来源',
   primary key (id),
   key idx_order_refund (out_trade_no, out_refund_no, deleted)
)
ENGINE = InnoDB AUTO_INCREMENT = 552 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '退款订单' ROW_FORMAT = Dynamic;

/*==============================================================*/
/* Table: kb_pay_order_refund_state                             */
/*==============================================================*/
create table kb_pay_order_refund_state
(
   id                   int(11) unsigned not null auto_increment comment '主键',
   deleted              char(1) character set utf8 comment '删除标志',
   create_time          datetime comment '创建时间',
   update_time          datetime comment '修改时间',
   inst_payee_account_no varchar(64) character set utf8 comment '支付机构收款账号',
   out_trade_no         varchar(64) character set utf8 comment '商户订单号',
   out_refund_no        varchar(64) character set utf8 comment '商户退款单号',
   state                varchar(15) character set utf8 comment '状态',
   state_msg            varchar(255) character set utf8 comment '支付平台状态描述',
   state_action         varchar(15) character set utf8 comment '状态改变动作',
   action_data          varchar(255) character set utf8 comment '状态改变数据',
   primary key (id),
   key idx_refund_state (out_trade_no, out_refund_no, deleted)
)
ENGINE = InnoDB AUTO_INCREMENT = 543 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '退款订单状态' ROW_FORMAT = Dynamic;

/*==============================================================*/
/* Table: kb_pay_order_result                                   */
/*==============================================================*/
create table kb_pay_order_result
(
   id                   bigint(20) unsigned not null auto_increment comment '主键',
   deleted              char(1) character set utf8,
   create_time          datetime,
   update_time          datetime,
   inst_payee_account_no varchar(64) character set utf8 comment '支付机构收款账号',
   pay_channel          varchar(10) character set utf8,
   out_trade_no         varchar(64) character set utf8,
   total_fee            mediumint(10) unsigned not null default 0 comment '支付金额,单位:分',
   contribute_amount    mediumint(10) unsigned not null default 0 comment '可优惠金额,单位:分',
   buyer_pay_amount     mediumint(10) unsigned not null comment '用户付款金额',
   trade_no             varchar(64) character set utf8,
   extra                varchar(255) character set utf8,
   openid               varchar(32) character set utf8,
   primary key (id),
   key idx_order_result (out_trade_no, deleted)
)
ENGINE = InnoDB AUTO_INCREMENT = 3874 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '支付结果' ROW_FORMAT = Dynamic;

/*==============================================================*/
/* Table: kb_pay_order_state                                    */
/*==============================================================*/
create table kb_pay_order_state
(
   id                   bigint(20) unsigned not null auto_increment comment '主键',
   deleted              char(1) character set utf8 comment '删除标志',
   create_time          datetime comment '创建时间',
   update_time          datetime comment '修改时间',
   inst_payee_account_no varchar(64) character set utf8 comment '支付机构收款账号',
   out_trade_no         varchar(64) character set utf8 comment '商户订单号',
   state                varchar(15) character set utf8 comment '支付订单状态',
   state_msg            varchar(255) character set utf8 comment '支付平台状态描述',
   state_action         varchar(15) character set utf8 comment '状态改变动作',
   action_data          varchar(255) character set utf8 comment '状态改变数据',
   primary key (id),
   key idx_state (out_trade_no, state, deleted)
)
ENGINE = InnoDB AUTO_INCREMENT = 9674 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '支付订单状态' ROW_FORMAT = Dynamic;

/*==============================================================*/
/* Table: kb_pay_refund_apply                                   */
/*==============================================================*/
create table kb_pay_refund_apply
(
   id                   int(11) unsigned not null auto_increment comment '主键',
   deleted              char(1) character set utf8 comment '删除标志',
   create_time          datetime comment '创建时间',
   update_time          datetime comment '修改时间',
   out_trade_no         varchar(64) character set utf8 comment '商户订单号',
   out_refund_no        varchar(64) character set utf8 comment '商户退款单号',
   refund_fee           int(10) unsigned not null default 0 comment '申请退款金额',
   total_fee            int(10) unsigned not null default 0 comment '支付金额,单位:分',
   refund_reason        varchar(255) character set utf8 comment '退款原因',
   audit_state          varchar(10) character set utf8 comment '审核状态',
   inst_payee_account_no varchar(64) character set utf8 comment '支付机构收款账号',
   primary key (id),
   key idx_refund_apply (out_trade_no, out_refund_no, deleted),
   key ids_apply_cds (create_time, audit_state)
)
ENGINE = InnoDB AUTO_INCREMENT = 356 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '退款订单' ROW_FORMAT = Dynamic;

/*==============================================================*/
/* Table: kb_pay_trade_no                                       */
/*==============================================================*/
create table kb_pay_trade_no
(
   id                   bigint(20) unsigned not null auto_increment comment '主键',
   deleted              char(1) character set utf8,
   create_time          datetime,
   update_time          datetime,
   inst_payee_account_no varchar(64) character set utf8 comment '支付机构收款账号',
   out_trade_no         varchar(64) character set utf8 comment '商户订单号',
   internal_trade_no    varchar(64) character set utf8,
   primary key (id),
   unique key trade_no_uk (internal_trade_no),
   key idx_tn_del_com_out (out_trade_no, deleted)
)
ENGINE = InnoDB AUTO_INCREMENT = 8474 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '支付订单状态' ROW_FORMAT = Dynamic;

/*==============================================================*/
/* Table: mch_p_order                                           */
/*==============================================================*/
create table mch_p_order
(
   id                   bigint(20) unsigned not null auto_increment,
   mch_no               varchar(64) character set utf8,
   channel              varchar(32) character set utf8,
   trade_type           varchar(32) character set utf8,
   out_trade_no         varchar(64) character set utf8,
   total_amount         int(11) not null,
   create_time          datetime,
   update_time          datetime,
   deleted              tinyint(1) not null,
   primary key (id)
)
ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

/*==============================================================*/
/* Table: mp_account                                            */
/*==============================================================*/
create table mp_account
(
   id                   int(11) unsigned not null auto_increment comment '主键',
   acc_name             varchar(100) character set utf8,
   state                char(1) character set utf8,
   update_time          timestamp(0) not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   create_time          datetime,
   deleted              char(1) character set utf8,
   merchant_code        varchar(64) character set utf8,
   primary key (id)
)
ENGINE = InnoDB AUTO_INCREMENT = 146 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '账号' ROW_FORMAT = Dynamic;

/*==============================================================*/
/* Table: mp_account_channel                                    */
/*==============================================================*/
create table mp_account_channel
(
   id                   int(11) unsigned not null auto_increment comment '主键',
   state                char(1) character set utf8,
   setting              text character set utf8,
   account_id           int(11) not null comment '收款账户ID',
   is_isv               int(11) default NULL,
   provider_id          int(11) not null comment '服务商或ISV的ID',
   create_time          datetime,
   update_time          timestamp(0) default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   deleted              char(1) character set utf8,
   channel_code         varchar(15) character set utf8,
   isv_id               int(11) default 0 comment 'ISV表的ID',
   certificate          text character set utf8,
   primary key (id)
)
ENGINE = InnoDB AUTO_INCREMENT = 296 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '账号-支付渠道配置' ROW_FORMAT = Dynamic;

/*==============================================================*/
/* Table: mp_account_store                                      */
/*==============================================================*/
create table mp_account_store
(
   id                   int(11) unsigned not null auto_increment comment '主键',
   store_name           varchar(100) character set utf8,
   store_code           varchar(64) character set utf8,
   merchant_code        varchar(64) character set utf8,
   merchant_name        varchar(100) character set utf8,
   account_id           int(11) not null comment '账号主键',
   create_time          datetime,
   update_time          datetime,
   deleted              char(1) character set utf8,
   primary key (id)
)
ENGINE = InnoDB AUTO_INCREMENT = 1300 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '账号-门店绑定关系' ROW_FORMAT = Dynamic;

/*==============================================================*/
/* Table: mp_aggregator                                         */
/*==============================================================*/
create table mp_aggregator
(
   id                   int(11) unsigned not null auto_increment comment '主键',
   aggregator_name      varchar(100) character set utf8,
   aggregator_code      varchar(20) character set utf8,
   create_time          datetime,
   update_time          timestamp(0) default NULL,
   deleted              char(1) character set utf8,
   primary key (id)
)
ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '聚合服务商' ROW_FORMAT = Dynamic;

/*==============================================================*/
/* Table: mp_channel                                            */
/*==============================================================*/
create table mp_channel
(
   id                   int(11) unsigned not null auto_increment comment '主键',
   channel_name         varchar(100) character set utf8,
   state                char(1) character set utf8,
   channel_code         varchar(20) character set utf8,
   is_discount          char(1) character set utf8,
   default_aggregator   varchar(20) character set utf8,
   start_bits           varchar(50) character set utf8,
   create_time          datetime,
   update_time          datetime,
   deleted              char(1) character set utf8,
   discount_code        varchar(100) character set utf8,
   default_isv          int(11) default -1 comment '\"-1\"标示没有默认的ISV',
   primary key (id)
)
ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '支付渠道' ROW_FORMAT = Dynamic;

/*==============================================================*/
/* Table: mp_channel_aggregator                                 */
/*==============================================================*/
create table mp_channel_aggregator
(
   id                   int(11) unsigned not null auto_increment comment '主键',
   channel_code         varchar(20) character set utf8,
   aggregator_code      varchar(20) character set utf8,
   create_time          datetime,
   update_time          datetime,
   deleted              char(1) character set utf8,
   primary key (id),
   unique key channel_provider_channel_code_provider_code_uindex (channel_code, aggregator_code)
)
ENGINE = InnoDB AUTO_INCREMENT = 51 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '聚合服务商-支付渠道关系' ROW_FORMAT = Dynamic;

/*==============================================================*/
/* Table: mp_isv                                                */
/*==============================================================*/
create table mp_isv
(
   id                   int(11) unsigned not null auto_increment comment '主键',
   channel_code         varchar(20) character set utf8,
   isv_name             varchar(200) character set utf8,
   appid                varchar(64) character set utf8,
   isv_code             varchar(32) character set utf8,
   isv_key              varchar(64) character set utf8,
   certificate          text character set utf8,
   uploadfile_time      datetime,
   deleted              char(1) character set utf8,
   create_time          datetime,
   update_time          datetime,
   primary key (id)
)
ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '微信服务商配置' ROW_FORMAT = Dynamic;

