/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2019/3/4 14:38:22                            */
/*==============================================================*/


drop table if exists tb_salary;

drop table if exists tb_user;

drop table if exists tb_work;

drop table if exists tb_work_count;

/*==============================================================*/
/* Table: tb_salary                                             */
/*==============================================================*/
create table tb_salary
(
   id                   varchar(36) not null comment 'ID',
   user_id              varchar(36) comment '用户ID',
   month                date comment '薪资月份',
   amt                  decimal(10,2) comment '总额',
   creater              varchar(36) comment '创建人',
   create_time          datetime comment '创建时间',
   modifier             varchar(36) comment '修改人',
   modify_time          datetime comment '修改时间',
   status               varchar(36) comment '状态',
   primary key (id)
);

/*==============================================================*/
/* Table: tb_user                                               */
/*==============================================================*/
create table tb_user
(
   id                   varchar(36) not null comment 'ID',
   username             varchar(36) comment '用户名',
   password             varchar(36) comment '密码',
   role                 varchar(12) comment '角色',
   phone                varchar(36) comment '手机号',
   nick                 varchar(12) comment '昵称',
   pic                  varchar(512) comment '头像',
   creater              varchar(36) comment '创建人',
   create_time          datetime comment '创建时间',
   modifier             varchar(36) comment '修改人',
   modify_time          datetime comment '修改时间',
   status               varchar(36) comment '状态',
   primary key (id)
);

/*==============================================================*/
/* Table: tb_work                                               */
/*==============================================================*/
create table tb_work
(
   id                   varchar(36) not null comment 'ID',
   user_id              varchar(36) comment '用户ID',
   card_time            datetime comment '打卡时间',
   type                 varchar(24) comment '打卡类别
            0-上班卡
            1-下班卡',
   is_arrive_later      char(8) comment '是否迟到',
   is_leave_early       char(8) comment '是否早退',
   work_status          char(24) comment '打卡状态
            0：未打卡
            1：打卡
            2：',
   creater              varchar(36) comment '创建人',
   create_time          datetime comment '创建时间',
   modifier             varchar(36) comment '修改人',
   modify_time          datetime comment '修改时间',
   status               varchar(36) comment '状态',
   primary key (id)
);

/*==============================================================*/
/* Table: tb_work_count                                         */
/*==============================================================*/
create table tb_work_count
(
   id                   varchar(36) not null comment 'ID',
   user_id              varchar(36) comment '用户ID',
   year                 varchar(4) comment '考勤年份',
   month                varchar(2) comment '考勤月份',
   work_day             int comment '出勤天数',
   rest_day             int comment '请假天数',
   arrive_late_day      int comment '迟到天数',
   leave_early          int comment '早退天数',
   creater              varchar(36) comment '创建人',
   create_time          datetime comment '创建时间',
   modifier             varchar(36) comment '修改人',
   modify_time          datetime comment '修改时间',
   status               varchar(36) comment '状态',
   primary key (id)
);

